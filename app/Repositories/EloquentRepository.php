<?php

namespace App\Repositories;

use App\Repositories\IEloquentRepository;

abstract class EloquentRepository implements IEloquentRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $_model;

    /**
     * EloquentRepository constructor.
     */
    public function __construct()
    {
        $this->setModel();
    }

    /**
     * get model
     * @return string
     */
    abstract public function getModel();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->_model = app()->make(
            $this->getModel()
        );
    }

    /**
     * Retrieve all data of repository
     * @param array | string $columns
     * @return mixed
     */
    public function getAll(array $columns = ['*'])
    {
        return $this->_model->all();
    }

    /**
     * Get one
     * @param int $id
     * @return mixed
     */
    public function findById(int $id)
    {
        return $this->_model->find($id);
    }

    /**
     * Get Paginate
     * @param int $limit
     * @param array | string $columns
     * @param string $pageName
     * @param int $page
     * @return mixed
     */
    public function paginate(int $limit = 0, $columns = ['*'], $pageName = 'page', $page = 0)
    {
        $limit = is_null($limit) ? 10 : $limit;
        if ($page != null) {
            return $this->model->paginate($limit, $columns, $pageName, $page);
        }
        return $this->model->paginate($limit, $columns);
    }

    /**
     * Create
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        return $this->_model->create($attributes);
    }

    /**
     * Update
     * @param int $id
     * @param array $attributes
     * @return mixed
     */
    public function update(int $id, array $attributes)
    {
        $result = $this->findById($id);
        if (!$result) return false;

        $result->update($attributes);

        return $result;
    }

    /**
     * Create or update a record matching the attributes, and fill it with values.
     * @param array $attributes
     * @param array $values
     * @return mixed
     */
    public function updateOrCreate(array $attributes, array $values = [])
    {
        $result = $this->_model->updateOrCreate($attributes, $values);
        return $result;
    }

    /**
     * Delete
     * @param int $id
     * @return mixed
     */
    public function delete(int $id)
    {
        $result = $this->findById($id);
        if (!$result) return false;

        return $result->delete();
    }

    /**
     * update via where condition
     * @param condition array .example: ['form_no' => '777A', ...]
     * @param data array .example('token_flag' => 1 ,...)
     * @param option string(operator '<', '>', '=', '<>')
     *
     * @return int result of the update query
     */
    public function updateViaWhereCondition($condition, $data = [], $option = '=')
    {
        if (!is_array($condition) || !is_array($data))
            return;

        $query = $this->_model->query();
        foreach ($condition as $key => $value) {
            $query->where($key, $option, $value);
        }
        return $query->update($data);
    }
}
