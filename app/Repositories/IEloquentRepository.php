<?php

namespace App\Repositories;

interface IEloquentRepository
{
    /**
     * Retrieve all data of repository
     * @param array | string $columns
     * @return mixed
     */
    public function getAll(array $columns = ['*']);

    /**
     * Get one
     * @param int $id
     * @return mixed
     */
    public function findById(int $id);

    /**
     * Get Paginate
     * @param int $limit
     * @param array | string $columns
     * @param string $pageName
     * @param int $page
     * @return mixed
     */
    public function paginate(int $limit = 0, $columns = ['*'], $pageName = 'page', $page = 0);

    /**
     * Create
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes);

    /**
     * Update
     * @param int $id
     * @param array $attributes
     * @return mixed
     */
    public function update(int $id, array $attributes);

    /**
     * Create or update a record matching the attributes, and fill it with values.
     * @param array $attributes
     * @param array $values
     * @return mixed
     */
    public function updateOrCreate(array $attributes, array $values = []);

    /**
     * Delete
     * @param int $id
     * @return mixed
     */
    public function delete(int $id);

    /**
     * update via where condition
     * @param condition array .example: ['form_no' => '777A', ...]
     * @param data array .example('token_flag' => 1 ,...)
     * @param option string(operator '<', '>', '=', '<>')
     *
     * @return int result of the update query
     */
    public function updateViaWhereCondition($condition, $data = [], $option = '=');
}
