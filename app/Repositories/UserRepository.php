<?php

namespace App\Repositories;

use App\Mail\VerifyEmail;
use App\Models\User;
use App\Traits\AdapterHelper;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Repositories\EloquentRepository;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserRepository extends EloquentRepository
{
    /**
     * getModel method
     *
     * @return string
     */
    public function getModel(): string
    {
        return User::class;
    }

    public function login($email, $password, $role)
    {
        $response = [
            'status' => true,
            'data' => null,
            'status_code' => 200,
            'message' => "Đăng nhập thành công!"
        ];
        $credentials = [
            'email' => $email,
            'password' => $password,
            'role' => $role
        ];

        $credentials['role'] = AdapterHelper::getTypeRole($role);
        if (!Auth::attempt($credentials)) {
            $response = [
                'status' => false,
                'data' => 'Error',
                'status_code' => 400,
                'message' => 'Email hoặc mật khẩu không chính xác. Vui lòng thử lại.'
            ];
            return $response;
        }

        $user = Auth::user();

        if (!isset($user->verified_at)) {
            $response['data']['user_id'] = $user->id; 
        }

        $response['data']['token'] = $user->createToken('Login Token')->accessToken;
        return $response;
    }

    public function register($data)
    {
        $input = $data;
        $input['password'] = bcrypt($input['password']);
        $user = $this->_model->create($input);
        $user->verify_code = ($user->id + 25) . Str::random(4);
        $user->verify_expired = strtotime(Carbon::now()->addDays(1));
        $user->save();
        Mail::to($user)->send(new VerifyEmail($user));

        return ['user_id' => $user->id, 'verify_code' => $user->verify_code];
    }

    /**
     * Sign out
     */
    public function logout($user)
    {
        $user->token()->revoke();
    }
}
