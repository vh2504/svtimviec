<?php

namespace App\Traits;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class AdapterHelper
{

    /**
     * @param $array
     * @param null $key
     * @return array
     */
    public static function unique_array($array, $key = null)
    {
        if (null === $key) {
            return array_unique($array);
        }
        $keys = [];
        $ret = [];
        foreach ($array as $elem) {
            $arrayKey = (is_array($elem)) ? $elem[$key] : $elem->$key;
            if (in_array($arrayKey, $keys)) {
                continue;
            }
            $ret[] = $elem;
            array_push($keys, $arrayKey);
        }
        return $ret;
    }

    public static function delete_file($path)
    {
        try {
            //code...
            $storage_path = $path;
            $tmp = explode(env("APP_URL") . "storage/", $path);
            if (count($tmp) == 2) {
                $storage_path = trim($tmp[1], '/');
            }
            if (Storage::disk('public')->exists($storage_path)) {
                return Storage::disk('public')->delete($storage_path);
                // return true;
            }
            return true;
        } catch (\Throwable $th) {
            throw $th;
            return false;
        }
    }

    // upload base 64
    public static function upload_file($file, $dir, $file_change = null)
    {
        try {
            //code...
            if ($file_change) {
                AdapterHelper::delete_file($file_change);
            }
            $tmp = explode(',', $file);
            if (count($tmp) < 2) {
                return null;
            }
            $exception = explode(';', $tmp[0]);
            $exception = explode('/', $exception[0]);
            $type_file = $exception[0];
            $exception = $exception[1];
            if ($type_file != 'data:image') {
                $final_exception = explode('.', $exception);
                if ($final_exception[count($final_exception) - 1] == 'sheet') {
                    $exception = 'xlsx';
                }
                if ($final_exception[count($final_exception) - 1] == 'document') {
                    $exception = 'docx';
                }
            }
            if ($exception == 'php')
                throw new Exception('Lỗi upload');
            $file = $tmp[1];
            $file = base64_decode($file);
            $dir .= '.' . $exception;
            $status = Storage::disk('public')->put($dir, $file);
            if (!$status) {
                throw new Exception('Lỗi upload ảnh');
            }
        } catch (\Throwable $th) {
            throw $th;
        }
        return $dir;
    }

    public static function getFirstCharacter($string)
    {
        // $string = AdapterHelper::createSlug($string);
        $string = static::url_slug($string);
        $string = trim($string, " ");
        $string = strtoupper($string);

        $res = $string ? $string[0] : "";
        for ($i = 1; $i < strlen($string); $i++) {
            if (($string[$i - 1] == ' ' && $string[$i] != ' ') || ($string[$i - 1] == '-' && $string[$i] != '-')) {
                $res .= "-" . $string[$i];
            }
        }
        $res = trim($res, " ");
        return $res;
    }

    public static function json_object_request($request)
    {
        return json_decode(json_encode($request->json()->all()));
    }

    public static function sendResponse($status, $data, $code, $message = 'None message')
    {
        if ($status === true)
            return response()->json([
                'status' => $status,
                'message' => $message,
                'data' => $data
            ], $code);
        return response()->json([
            'status' => $status,
            'error' => $data,
            'message' => $message
        ], $code);
    }

    public static function sendResponseError($validator)
    {
        return self::sendResponse(
            false,
            isset($validator['error']) ? $validator['error'] : 'errors',
            isset($validator['code']) ? $validator['code'] : 400,
            $validator['message']
        );
    }

    public static function sendResponseSuccess($data)
    {
        return self::sendResponse(
            true,
            $data,
            200,
            'success'
        );
    }

    public static  function write_log_error($exception, $device, $url, $channel = 'state')
    {
        $message = "\n ------------" . date('H:i:s') . "-" . $device . "-------------\n";
        $message .= "url: " . $url;
        if (method_exists($exception, 'getMessage'))
            $message .= "\nMessage: " . $exception->getMessage();
        if (method_exists($exception, 'getFile'))
            $message .= "\nFile: " . $exception->getFile();
        if (method_exists($exception, 'getLine'))
            $message .= "\nLine: " . $exception->getLine();
        $message .= "\n ------------end------------\n";
        Log::stack([$channel])->info($message);
    }

    public static function ViewAndLogError($exception, $request)
    {
        static::write_log_error($exception, "Mobile", $request->getRequestUri());
        return static::sendResponse(false, 'Undefined Error', 500, $exception->getMessage());
    }
    public static function sendResponsePaginating($status = true, $data, $code, $message = 'None message')
    {
        $data = json_decode($data->toJson());

        $payload = $data->data;
        $temp = [];
        if (is_object($payload)) {
            foreach ($payload as $p) {
                $temp[] = $p;
            }
            $payload = $temp;
        }

        $pagination = array([
            "current_page" =>  $data->current_page,
            "from_record" =>  $data->from,
            "to_record" =>  $data->to,
            "total_record" =>  $data->total,
            "record_per_page" =>  (int) $data->per_page,
            "total_page" =>  $data->last_page,
        ])[0];
        return response()->json(
            [
                'status' => $status,
                'message' => $message,
                'pagination' => $pagination,
                'data' => $payload,
            ],
            $code
        );
    }

    public static function sendResponsePaginatingV2($status = true, $data, $code, $message = 'None message')
    {
        $payload = $data->items();
        $pagination = [
            "current_page" =>  $data->currentPage(),
            "from_record" =>  $data->firstItem(),
            "to_record" =>  $data->lastItem(),
            "total_record" =>  $data->total(),
            "record_per_page" =>  (int) $data->perPage(),
            "total_page" =>  $data->lastPage(),
        ];
        return response()->json(
            [
                'status' => $status,
                'message' => $message,
                'pagination' => $pagination,
                'data' => $payload,
            ],
            $code
        );
    }

    public static function sendResponsePaginate($status = true, $data, $code, $message = 'None message')
    {

        return response()->json(
            [
                'status' => $status,
                'message' => $message,
                'pagination' => $data['pagination'],
                'data' => $data['data'],
            ],
            $code
        );
    }

    public static function getDataPagination($data)
    {

        $payload = $data->items();
        $data = json_decode($data->toJson());
        $pagination = [
            "current_page" =>  $data->current_page,
            "from_record" =>  $data->from,
            "to_record" =>  $data->to,
            "total_record" =>  $data->total,
            "record_per_page" =>  (int) $data->per_page,
            "total_page" =>  $data->last_page,
        ];
        return [
            'data' => $payload,
            'pagination' => $pagination
        ];
    }


    /**
     * @param $stringRole string
     * @return int
     */
    public static function getTypeRole(string $stringRole): int
    {
        $role = 0;
        switch ($stringRole) {
            case 'ADMIN':
                $role = 1;
                break;
            case 'RECRUITER':
                $role = 2;
                break;
            case 'STUDENT':
                $role = 3;
                break;
        }

        return $role;
    }

    public static function upload_image($image, $dirSave, $diskName)
    {
        $extension = $image->getClientOriginalExtension();

        if (!in_array($extension, ['jpg', 'jpeg', 'png', 'psd'])) {
            return false;
        }

        $disk = Storage::disk($diskName);
        $fileName = time() . '.' . $extension;

        $storeLink = $disk->putFileAs($dirSave, $image, $fileName);

        return $disk->url($storeLink);

        // $savePath = $image->storeAs($dirSave, time() . '.' . $extension, $diskName);
        // return $disk->url( $savePath);
    }
}
