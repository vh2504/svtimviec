<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Traits\AdapterHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    protected  $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'type_role' => 'required',
        ]);

        if ($validator->fails()) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, $validator->errors()->first());
        }

        if (!in_array($request->type_role, User::$const_type_role)) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, 'type_role không hỗ trợ.');
        }

        $response = $this->userRepo->login($request->email, $request->password, $request->type_role);
        return AdapterHelper::sendResponse($response['status'], $response['data'], $response['status_code'], $response['message']);
    }

    public function logout(Request $request)
    {
        $this->userRepo->logout($request->user());
        return AdapterHelper::sendResponse(true, 'Logout successfully', 200, "Đăng xuất thành công.");
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8',
            'confirm_password' => 'required|min:8',
            'name' => 'required',
            'type_role' => 'required'
        ]);

        if ($validator->fails()) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, $validator->errors()->first());
        }

        $list_role = User::$const_type_role;
        unset($list_role['ADMIN']);

        if (!in_array($request->type_role, $list_role)) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, 'type_role không hỗ trợ.');
        }

        $check_email_unique = User::query()->where('email', $request->email)->exists();

        if ($check_email_unique) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, 'Email đã tồn tại trong hệ thống.');
        }

        if ($request->password) {
            if (strlen($request->password) < 8) {
                return AdapterHelper::sendResponse(false, 'Validator error', 400, 'Độ dài của mật khẩu từ 8 chữ số.');
            }
            if ($request->password != $request->confirm_password) {
                return AdapterHelper::sendResponse(false, 'Validator error', 400, 'Mật khẩu và nhập lại mật khẩu không chính xác.');
            }
        }

        $request->merge([
            'role' => AdapterHelper::getTypeRole($request->type_role)
        ]);
        $input = $request->all();
        $data = $this->userRepo->register($input);

        return AdapterHelper::sendResponse(true, $data, 200, 'Đăng ký tài khoản thành công.');
    }

    public function confirm_email(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verify_code' => 'required',
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, $validator->errors()->first());
        }

        $user = User::query()->where('verify_code', $request->verify_code)->find($request->user_id);
        if (!$user) {
            return AdapterHelper::sendResponse(false, 'Error verify code fail.', 400, 'verify_code không khớp.');
        }
        if ($user->verified_at !== null) {
            return AdapterHelper::sendResponse(false, 'verified', 400, "Tài khoản của bạn đã được xác thực. Vui lòng đăng nhập để tiếp tục sử dụng.");
        }
        if (strtotime($user->verify_expired) < time()) {
            return AdapterHelper::sendResponse(false, 'error expired', 400, "Mã xác thực đã hết hạn.");
        }

        $date = date("Y-m-d H:i:s");
        $user->verified_at = $date;
        $user->save();

        return AdapterHelper::sendResponse(true, 'success', 200, "Xác thực tài khoản thành công.");
    }
}
