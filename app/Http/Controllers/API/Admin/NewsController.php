<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\FavoriteNews;
use App\Models\Recruitment;
use App\Models\RecruitmentNews;
use App\Models\ReportNews;
use App\Traits\AdapterHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    public function getAll(Request $request)
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xem tin tuyển dụng.");
        }

        $all = RecruitmentNews::query();
        if (isset($request->search)) {
            $all = $all->with('recruiter.user')
                ->orWhereHas('recruiter', function ($q) use ($request) {
                    $q = $q->where('company_name', 'like', '%' .$request->search. '%');
                })
                ->orWhere('job_name', 'like', '%' .$request->search. '%');
        } else {
            $all = $all->with('recruiter.user');
        }
        
        $all = $all->orderByDesc('created_at')
            ->paginate($request->per_page ?? config('app.per_page'));

        return AdapterHelper::sendResponsePaginating(true, $all, 200, "success");
    }

    public function detail(Request $request, $id)
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xem tin tuyển dụng.");
        }

        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }

        $news = RecruitmentNews::query()
            ->with('recruiter.user')
            ->where('id', $id)->first();

        return AdapterHelper::sendResponse(true, $news, 200, "Success.");
    }

    public function delete(Request $request, $id)
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xoá tin tuyển dụng.");
        }

        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }
        DB::beginTransaction();
        try {
            $favorite = FavoriteNews::where('recruitment_new_id', $id)->first();
            $favorite ? $favorite->delete() : '';

            $report = ReportNews::where('recruitment_new_id', $id)->first();
            $report ? $report->delete() : '';

            $recruitment_apply = Recruitment::where('recruitment_new_id', $id)->first();
            $recruitment_apply ? $recruitment_apply->delete() : '';

            $recruitment_news->delete();

            DB::commit();
            return AdapterHelper::sendResponse(true, 'success', 200, "Xoá tin tuyển dụng thành công.");
        } catch (\Throwable $th) {
            return AdapterHelper::sendResponse(false, 'error', 400, "Đã có lỗi xảy ra.");
            // throw $th;
            DB::rollback();
        }
    }

    public function approve(Request $request, $id)
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xoá tin tuyển dụng.");
        }

        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }
        DB::beginTransaction();
        try {
            if ($recruitment_news->status == 1) {
                return AdapterHelper::sendResponse(false, 'error approve', 400, "Tin tuyển dụng này đã được duyệt.");
            }

            if (isset($recruitment_news->jobCategory->is_publised) && $recruitment_news->jobCategory->is_publised == 0) {
                $recruitment_news->jobCategory->is_publised = 1;
                $recruitment_news->jobCategory->save();
            }

            $recruitment_news->status = 1;
            $recruitment_news->admin_id = $user->admin->admin_id;
            $recruitment_news->save();

            DB::commit();
            return AdapterHelper::sendResponse(true, 'success', 200, "Duyệt tin tuyển dụng thành công.");

        } catch (\Throwable $th) {
            return AdapterHelper::sendResponse(false, 'error', 400, "Đã có lỗi xảy ra.");
            // throw $th;
            DB::rollback();
        }
    }
}
