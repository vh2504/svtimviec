<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\Recruiter;
use App\Models\User;
use App\Traits\AdapterHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RecruiterController extends Controller
{
    public function create(Request $request)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'company_name' => 'required',
            'company_address' => 'required',
            'provinceID' => 'required'
        ]);

        if ($validator->fails()) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, $validator->errors()->first());
        }

        $check = User::where('email', $request->email)->first();
        if($check) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Email đã tồn tại trong hệ thống");
        }

        $user = User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'name' => $request->name,
            'role' => 2,
            'verify_code' => 'admin' . $admin->id,
            'verified_at' => time()
        ]);

        Recruiter::create([
            'user_id' => $user->id,
            'phone' => $request->phone,
            'company_name' => $request->company_name,
            'company_address' => $request->company_address,
            'province_id' => $request->provinceID ?? 223
        ]);

        return AdapterHelper::sendResponse(true, 'success', 200, 'Thêm mới sinh viên thành công.');
    }

    public function get_info_recruiter(Request $request, $id)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $check = User::where('role', 2)->find($id);
        if(!$check) {
            return AdapterHelper::sendResponse(false, 'not found error', 400, "Không tìm thấy nhà tuyển dụng này");
        }

        $recruiter = User::query()
            ->join('recruiters', 'user_id', 'id')
            ->where('role', 2)->find($id);

        return AdapterHelper::sendResponse(true, $recruiter, 200, 'Lấy thông tin nhà tuyển dụng thành công');
    }

    public function delete(Request $request, $id)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $check = User::where('role', 2)->find($id);
        if(!$check) {
            return AdapterHelper::sendResponse(false, 'not found error', 400, "Không tìm thấy nhà tuyển dụng này");
        }

        User::where('role', 2)->find($id)->recruiter->delete();
        User::where('role', 2)->find($id)->delete();

        return AdapterHelper::sendResponse(true, 'success', 200, 'Xoá nhà tuyển dụng thành công');
    }

    public function block(Request $request, $id)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $check = User::where('role', 2)->find($id);
        if(!$check) {
            return AdapterHelper::sendResponse(false, 'not found error', 400, "Không tìm thấy nhà tuyển dụng này");
        }

        User::where('role', 2)->find($id)->recruiter->update([
            'is_block' => 1
        ]);

        return AdapterHelper::sendResponse(true, 'success', 200, 'Khoá tài khoản nhà tuyển dụng thành công');
    }

    public function get_all(Request $request)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $all = User::query();
        if (isset($request->search)) {
            $all = $all
                ->join('recruiters', 'user_id', 'id')
                ->where('name', 'like', '%' .$request->search. '%')
                ->where('role', 2);
        } else {
            $all = $all->join('recruiters', 'user_id', 'id')
                ->where('role', 2);
        }
        
        $all = $all->orderByDesc('created_at')
            ->paginate($request->per_page ?? config('app.per_page'));

        return AdapterHelper::sendResponsePaginating(true, $all, 200, "success");
    }
}
