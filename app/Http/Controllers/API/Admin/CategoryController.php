<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\JobCategory;
use App\Models\RecruitmentNews;
use App\Traits\AdapterHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function getAll (Request $request)
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền.");
        }

        $all = JobCategory::query();

        if (isset($request->search)) {
            $all = $all->where('name', 'like', '%' . $request->search. '%');
        } 
        
        $all = $all->paginate($request->per_page ?? config('app.per_page'));

        return AdapterHelper::sendResponsePaginating(true, $all, 200, "Lấy danh sách danh mục công việc thành công");
    }

    public function create(Request $request)
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền.");
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, $validator->errors()->first());
        }

        DB::beginTransaction();
        try {
            JobCategory::create([
                'name' => $request->name,
                'description' => $request->description,
                'is_publised' => 1
            ]);

            DB::commit();
            return AdapterHelper::sendResponse(true, 'success', 200, "Thêm mới danh mục công việc thành công.");

        } catch (\Throwable $th) {
            return AdapterHelper::sendResponse(false, 'error', 400, "Đã có lỗi xảy ra.");
            // throw $th;
            DB::rollback();
        }
    }

    public function detail(Request $request, $id)
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền.");
        }

        $cate = JobCategory::query()
            ->with('recruitmentNew.recruiter.user')
            ->find($id);

        if (!$cate) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy danh mục.");
        }

        return AdapterHelper::sendResponse(true, $cate, 200, "Xem chi tiết danh mục thành công.");
    }

    public function delete(Request $request, $id)
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền.");
        }

        $cate = JobCategory::query()->find($id);

        if (!$cate) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy danh mục.");
        }

        if ($cate->recruitmentNew->count() > 0) {
            RecruitmentNews::where('job_category_id', $id)->delete();
        }
        
        $cate->delete();

        return AdapterHelper::sendResponse(true, $cate, 200, "Xoá danh mục thành công.");
    }
}
