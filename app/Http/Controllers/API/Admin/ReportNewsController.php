<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\RecruitmentNews;
use App\Models\ReportNews;
use App\Traits\AdapterHelper;
use Illuminate\Http\Request;

class ReportNewsController extends Controller
{
    public function getAll(Request $request) 
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xem tin tuyển dụng.");
        }

        $all = RecruitmentNews::query()
            ->withCount('report_news')
            ->with('recruiter.user')
            ->whereHas('report_news');
    
        $all = $all->orderByDesc('created_at')
            ->paginate($request->per_page ?? config('app.per_page'));

        return AdapterHelper::sendResponsePaginating(true, $all, 200, "Lấy danh sách tin bị report thành công.");
    }

    public function detail(Request $request, $id)
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xem tin tuyển dụng.");
        }

        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }

        $news = RecruitmentNews::query()
            ->with('recruiter.user')
            ->with('report_news')
            ->whereHas('report_news')
            ->find($id);

        if (!$news) {
            return AdapterHelper::sendResponse(false, null, 400, "Không có tin này trong danh sách report.");
        }

        return AdapterHelper::sendResponse(true, $news, 200, "Xem chi tiết tin report thành công.");
    }

    // id của report news
    public function delete_report(Request $request, $id)
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xem tin tuyển dụng.");
        }

        $recruitment_news = ReportNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy report này.");
        }

        $recruitment_news->delete();
        
        return AdapterHelper::sendResponse(true, 'success', 200, "Xoá tin tuyển dụng khỏi danh sách bị report thành công.");
    }
}
