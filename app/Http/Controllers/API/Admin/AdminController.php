<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\User;
use App\Traits\AdapterHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    //Lấy thông tin admin
    public function get_admin_info(Request $request)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $admin = User::query()
            ->join('admins', 'user_id', 'id')
            ->find($admin->id);

        return AdapterHelper::sendResponse(true, $admin, 200, 'Lấy thông tin admin thành công.');
    }

    //Tạo admin
    public function add_admin(Request $request)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, $validator->errors()->first());
        }

        $check = User::where('email', $request->email)->first();
        if($check) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Email đã tồn tại trong hệ thống");
        }

        $user = User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'name' => $request->name, 
            'role' => 1
        ]);

        Admin::create([
            'master_role' => 0,
            'user_id' => $user->id
        ]);

        return AdapterHelper::sendResponse(true, 'success', 200, 'Thêm mới admin thành công.');
    }


    //Quên mật khẩu
}
