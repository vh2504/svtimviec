<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\User;
use App\Traits\AdapterHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    public function create(Request $request)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'age' => 'required',
            'sex' => 'required',
            'provinceID' => 'required'
        ]);

        if ($validator->fails()) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, $validator->errors()->first());
        }

        $check = User::where('email', $request->email)->first();
        if($check) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Email đã tồn tại trong hệ thống");
        }

        $user = User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'name' => $request->name,
            'role' => 3,
            'verify_code' => 'admin' . $admin->id,
            'verified_at' => time()
        ]);

        Student::create([
            'user_id' => $user->id,
            'phone' => $request->phone,
            'age' => $request->age,
            'sex' => $request->sex,
            'address' => $request->address,
            'province_id' => $request->provinceID ?? 223
        ]);

        return AdapterHelper::sendResponse(true, 'success', 200, 'Thêm mới sinh viên thành công.');
    }

    public function get_info_student(Request $request, $id)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $check = User::where('role', 3)->find($id);
        if(!$check) {
            return AdapterHelper::sendResponse(false, 'not found error', 400, "Không tìm thấy sinh viên này");
        }

        $student = User::query()
            ->join('students', 'user_id', 'id')
            ->where('role', 3)->find($id);

        return AdapterHelper::sendResponse(true, $student, 200, 'Lấy thông tin sinh viên thành công');
    }

    public function delete(Request $request, $id)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $check = User::where('role', 3)->find($id);
        if(!$check) {
            return AdapterHelper::sendResponse(false, 'not found error', 400, "Không tìm thấy sinh viên này");
        }

        User::where('role', 3)->find($id)->student->delete();
        User::where('role', 3)->find($id)->delete();

        return AdapterHelper::sendResponse(true, 'success', 200, 'Xoá sinh viên thành công');
    }

    public function block(Request $request, $id)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $check = User::where('role', 3)->find($id);
        if(!$check) {
            return AdapterHelper::sendResponse(false, 'not found error', 400, "Không tìm thấy sinh viên này");
        }

        User::where('role', 3)->find($id)->student->update([
            'is_block' => 1
        ]);

        return AdapterHelper::sendResponse(true, 'success', 200, 'Khoá tài khoản sinh viên thành công');
    }

    public function get_all(Request $request)
    {
        $admin = $request->user();
        
        if ($admin->role != 1) {
            return AdapterHelper::sendResponse(false, 'unauthorization', 400, 'Không có quyền.');
        }

        $all = User::query();
        if (isset($request->search)) {
            $all = $all
                ->join('students', 'user_id', 'id')
                ->where('name', 'like', '%' .$request->search. '%')
                ->where('role', 3);
        } else {
            $all = $all->join('students', 'user_id', 'id')
                ->where('role', 3);
        }
        
        $all = $all->orderBy('id', 'asc')
            ->paginate($request->per_page ?? config('app.per_page'));

        return AdapterHelper::sendResponsePaginating(true, $all, 200, "success");
    }
}
