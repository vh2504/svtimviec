<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\FavoriteNews;
use App\Models\JobCategory;
use App\Models\Recruitment;
use App\Models\RecruitmentNews;
use App\Models\ReportNews;
use App\Traits\AdapterHelper;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RecruitmentNewsController extends Controller
{

    // Tạo tin tuyển dụng
    public function create(Request $request)
    {
        if ($request->user()->role != 2) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền tạo tin tuyển dụng.");
        }

        $validator = Validator::make($request->all(), [
            'job_name' => 'required',
            'job_description' => 'required',
            'is_new_category' => 'required',
            'job_category_id' => $request->is_new_category == 0 ? 'required' : '',
            'job_category_name' =>  $request->is_new_category == 1 ? 'required' : '',
            'job_category_description' =>  $request->is_new_category == 1 ? 'required' : '',
            'salary_type' => 'required',
            'salary_from' => $request->salary_type == 1 ? 'required': '',
            'salary_to' => $request->salary_type == 1 ? 'required': '',
        ]);

        if ($validator->fails()) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, $validator->errors()->first());
        }

        DB::beginTransaction();
        try {
            if ($request->is_new_category == 1) {
                $newCategory = JobCategory::create([
                    'name' => $request->job_category_name ?? '',
                    'description' => $request->job_category_description ?? '',
                    'is_publised' => 0
                ]);
            }
            if ($request->is_new_category == 1 && !isset($newCategory)) {
                return AdapterHelper::sendResponse(false, 'Error', 400, "Có lỗi xảy ra.");
            }

            $news = RecruitmentNews::create([
                'job_name' => $request->job_name,
                'job_description' => $request->job_description,
                'job_category_id' => $request->is_new_category == 1 ? $newCategory->id : $request->job_category_id,
                'salary_type' => $request->salary_type, // 1 hoặc 2 (cố định hoặc thoả thuận)
                'recruiter_id' => $request->user()->recruiter->recruiter_id,
                'status' => 0, // 0 chưa duyệt, 1 đã duyệt
                'min_age' => $request->min_age ?? null,
                'sex' => $request->sex ?? 2 // mặc định tạo ra không yêu cầu giới tính
            ]);

            if ($request->salary_type == 1) {
                $news->salary_from = $request->salary_from;
                $news->salary_to = $request->salary_to;
                $news->save();
            }
            DB::commit();
            return AdapterHelper::sendResponse(true, 'success', 200, "Tạo tin tuyển dụng thành công. Vui lòng chờ admin duyệt!");
        } catch (Exception $e) {
            DB::rollback();
            AdapterHelper::write_log_error($e, 'api', 'create news');
            return AdapterHelper::sendResponse(false, 'Error', 400, "Có lỗi xảy ra khi tạo tin tuyển dụng.");
        }
    }

    // Chỉnh sửa tin tuyển dụng
    public function edit (Request $request, $id)
    {
        if ($request->user()->role != 2) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền chỉnh sửa tin tuyển dụng.");
        }

        $news = RecruitmentNews::where('recruiter_id', $request->user()->recruiter->recruiter_id)
            ->find($id);
            
        if (!$news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }

        DB::beginTransaction();
        try {
            if ($request->is_new_category == 1) {
                $newCategory = JobCategory::create([
                    'name' => $request->job_category_name ?? '',
                    'description' => $request->job_category_description ?? '',
                    'is_publised' => 0
                ]);
            }
            if ($request->is_new_category == 1 && !isset($newCategory)) {
                return AdapterHelper::sendResponse(false, 'Error', 400, "Có lỗi xảy ra.");
            }

            RecruitmentNews::find($id)->update([
                'job_name' => $request->job_name ?? $news->job_name,
                'job_description' => $request->job_description ?? $news->job_description,
                'job_category_id' => $request->is_new_category == 1 ? $newCategory->id : $request->job_category_id ?? $news->job_category_id,
                'salary_type' => $request->salary_type ?? $news->salary_type, // 1 hoặc 2 (cố định hoặc thoả thuận)
                'recruiter_id' => $request->user()->recruiter->recruiter_id,
                // 'status' => 0, // 0 chưa duyệt, 1 đã duyệt
                'min_age' => $request->min_age ?? $news->min_age,
                'sex' => $request->sex ?? $news->sex, // mặc định tạo ra không yêu cầu giới tính
            ]);

            if ($request->salary_type == 1) {
                $news->salary_from = $request->salary_from ?? $news->salary_from;
                $news->salary_to = $request->salary_to ?? $news->salary_to;
                $news->save();
            } else {
                $news->salary_from = null;
                $news->salary_to = null;
                $news->save(); 
            }
            DB::commit();

            return AdapterHelper::sendResponse(true, 'success', 200, "Chỉnh sửa tin tuyển dụng thành công.");
        } catch (Exception $e) {
            DB::rollback();

            AdapterHelper::write_log_error($e, 'api', 'edit news');
            return AdapterHelper::sendResponse(false, 'Error', 400, "Có lỗi xảy ra khi chỉnh sửa tin tuyển dụng.");
        }
    }

    // lịch sử đăng tin của nhà tuyển dụng
    public function get_history_news(Request $request)
    {
        $user = $request->user();
        if (!$this->isRecuiter($request)) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xem lịch sử đăng tin tin tuyển dụng.");
        }

        $news = RecruitmentNews::query()
            ->with('recruiter', function ($q) {
                $q->join('users', 'user_id', 'id')
                    ->select('recruiters.*', 'id', 'email', 'name', 'avatar', 'role');
            })
            ->where('recruiter_id', $user->recruiter->recruiter_id);
            // ->where('status', 1); // k cần vì có trường hợp chưa được duyệt

        $news = $news->orderByDesc('created_at')
            ->paginate($request->per_page ?? config('app.per_page'));

        return AdapterHelper::sendResponsePaginating(true, $news, 200, "success");
    }

    //xem chi tiết tin tuyển dụng cho nhà tuyển dụng
    public function detail_news(Request $request, $id)
    {
        $user = $request->user();
        if ($user->role != 2) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xem chi tiết tin tuyển dụng.");
        }

        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }

        $news = RecruitmentNews::query()
                ->with('recruiter', function ($q) {
                    $q->join('users', 'user_id', 'id')->select('recruiters.*', 'id', 'email', 'name', 'avatar', 'role');
                })
                ->with('jobCategory')
                ->withCount('students_apply')
                ->where('recruiter_id', $user->recruiter->recruiter_id)
                ->find($id);

        if (!$news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng phù hợp.");
        }

        return AdapterHelper::sendResponse(true, $news, 200, "Success.");
    }

    //nhà tuyển dụng xoá tin tuyển dụng
    public function delete_news(Request $request, $id)
    {
        $user = $request->user();
        if ($user->role != 2) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xoá tin tuyển dụng.");
        }

        $recruitment_news = RecruitmentNews::query()
            ->whereHas('recruiter', function ($q) use ($user) {
                $q->where('recruiter_id', $user->recruiter->recruiter_id);
            })
            ->find($id);

            if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }

        DB::beginTransaction();
        try {
            $favorite = FavoriteNews::where('recruitment_new_id', $id)->first();
            $favorite ? $favorite->delete() : '';

            $report = ReportNews::where('recruitment_new_id', $id)->first();
            $report ? $report->delete() : '';

            $recruitment_apply = Recruitment::where('recruitment_new_id', $id)->first();
            $recruitment_apply ? $recruitment_apply->delete() : '';

            $recruitment_news->delete();

            DB::commit();
            return AdapterHelper::sendResponse(true, 'success', 200, "Xoá tin tuyển dụng thành công.");
        } catch (\Throwable $th) {
            return AdapterHelper::sendResponse(false, 'error', 400, "Đã có lỗi xảy ra.");
            DB::rollback();
        }
    }

    //Lấy tất cả tin
    public function getAll(Request $request)
    {
        $user = $request->user();
        if ($user->role != 3) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xem tin tuyển dụng.");
        }

        $news = RecruitmentNews::query()
                ->with('recruiter', function ($q) {
                    $q->join('users', 'user_id', 'id')->select('recruiters.*', 'id', 'email', 'name', 'avatar', 'role');
                })
                ->with('apply_news', function ($q) use ($user) {
                    $q->where('student_id', $user->student->student_id);
                })
                ->with('favorite_news', function ($q) use ($user) {
                    $q->where('student_id', $user->student->student_id);
                })
                ->with('report_news', function ($q) use ($user) {
                    $q->where('student_id', $user->student->student_id);
                })
                ->where('status', 1);

        if (isset($request->search)) {
            $news->where('job_name','like', '%'. $request->search. '%')
                ->orWhere('job_description','like', '%'. $request->search. '%');

            $news->orWhereHas('recruiter', function($q) use ($request){
                $q->where('company_name','like', '%'. $request->search. '%');
            });
        }

        if (isset($request->province_id)) {
            $news->whereHas('recruiter', function($q) use ($request){
                $q->where('province_id', $request->province_id);
            });
        }

        if (isset($request->salary_type)) {
            if ($request->salary_type == 1) {
                $news->where('salary_type', 1);

                $news = isset($request->salary_from) && !isset($request->salary_to) ? $news->where('salary_from', '>=', $request->salary_from ?? 0) : $news;
                $news = isset($request->salary_to) && !isset($request->salary_from) && $request->salary_to > 0 ? $news->where('salary_to', '>=', $request->salary_to) : $news;
                $news = isset($request->salary_from) && isset($request->salary_to) && $request->salary_from <= $request->salary_to 
                    ? $news->where('salary_from', '>=', $request->salary_from ?? 0)
                        ->where('salary_to' , '<=', $request->salary_to)
                    : $news;
            } else if ($request->salary_type == 2) {
                $news->where('salary_type', 2);
            }
        }

        if (isset($request->min_age)) {
            $news->where('min_age', '>=', $request->min_age);
        }

        if (isset($request->sex)) {
            if ($request->sex == 2) {
                $news->where(function($q) {
                    $q->where('sex', 0)
                        ->orWhere('sex', 1)
                        ->orWhere('sex', 2);
                });
            } else {
                $news->where('sex', $request->sex);
            }
        }

        $news = $news->orderByDesc('created_at')
            ->paginate($request->per_page ?? config('app.per_page'));

        foreach ($news as $data) {
            $data->is_favorite = isset($data->favorite_news) && $data->favorite_news->count() > 0 ? 1 : 0;
            $data->is_reported = isset($data->report_news) && $data->report_news->count() > 0 ? 1 : 0;
            $data->is_applied = isset($data->apply_news) && $data->apply_news->count() > 0 ? 1 : 0;
        }

        return AdapterHelper::sendResponsePaginating(true, $news, 200, "success");
    }

    public function detail(Request $request, $id)
    {
        $user = $request->user();
        if ($user->role != 3) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xem tin tuyển dụng.");
        }

        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }

        $news = RecruitmentNews::query()
                ->with('recruiter', function ($q) {
                    $q->join('users', 'user_id', 'id')->select('recruiters.*', 'id', 'email', 'name', 'avatar', 'role');
                })
                ->with('report_news', function ($q) use ($user) {
                    $q->join('students', 'report_news.student_id', 'students.student_id')
                        ->join('users', 'students.user_id', 'users.id')
                        ->select('report_news.*', 'name', 'avatar');
                })
                ->with('apply_news', function ($q) use ($user) {
                    $q->where('student_id', $user->student->student_id);
                })
                ->where('id', $id)->first();

        if (!$news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng phù hợp.");
        }

        $news->is_favorite = 0;
        $news->is_applied = 0;
        $countReport = $news->report_news->where('student_id', $user->student->student_id)->count();
        $news->count_report = $news->report_news->count();

        if ($countReport > 0) {
            $news->is_reported = 1;
        }
        if ($news->favorite_news->where('student_id', $user->student->student_id)->count() > 0) {
            $news->is_favorite = 1;
        }
        if (isset($news->apply_news) && $news->apply_news->count() > 0) {
            $news->is_applied = 1;
        }
        return AdapterHelper::sendResponse(true, $news, 200, "Success.");
    }

    //ứng tuyển vào tin tuyển dụng
    public function apply(Request $request)
    {
        if (!isset($request->id)) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "id là trường bắt buộc");
        }
        $id = $request->id;

        if (!$this->isStudent($request)) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Bạn không có quyền để ứng tuyển vào tin tuyển dụng.");
        }

        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }

        $student = $request->user()->student;
        $checkApply = $student->apply_recruitment_news->where('id', $id)->count() == 1;

        if ($checkApply) {
            return AdapterHelper::sendResponse(false, 'applied', 400, "Bạn đã ứng tuyển vào tin tuyển dụng này.");
        }

        $student->apply_recruitment_news()->attach($id);
        return AdapterHelper::sendResponse(true, 'success', 200, "Ứng tuyển thành công.");

    }

    //id tin tuyển dụng
    public function report(Request $request, $id)
    {
        if (!$this->isStudent($request)) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Bạn không có quyền để report tin tuyển dụng.");
        }

        $validator = Validator::make($request->all(), [
            'reason' => 'required'
        ]);

        if ($validator->fails()) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, $validator->errors()->first());
        }

        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }

        $student = $request->user()->student;
        $checkReport = $student->report_news->where('recruitment_new_id', $id)->count();
        if ($checkReport > 0) {
            return AdapterHelper::sendResponse(false, 'reported', 400, "Bạn đã report tin tuyển dụng này.");
        }
        ReportNews::create([
            'student_id' => $student->student_id,
            'recruitment_new_id' => $id,
            'reason' => $request->reason
        ]);

        $countReport = ReportNews::where('recruitment_new_id', $id)->count();
        if ($countReport > 15) {
            $student->is_block = 1;
            $student->save();
        }
        return AdapterHelper::sendResponse(true, 'success', 200, "Report thành công.");
    }


    public function get_all_favorite_news(Request $request)
    {
        if (!$this->isStudent($request)) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Bạn không có quyền để xem tin tuyển dụng yêu thích.");
        }

        $student = $request->user()->student;
        $news = RecruitmentNews::query()
            ->with('recruiter', function ($q) {
                $q->join('users', 'user_id', 'id')->select('recruiters.*', 'id', 'email', 'name', 'avatar', 'role');
            })
            ->whereHas('favorite_news', function($q) use ($student) {
                $q->where('student_id', $student->student_id);
            })
            ->paginate($request->per_page ?? config('app.per_page'));

        return AdapterHelper::sendResponsePaginating(true, $news, 200, "success");
    }

    //id tin tuyển dụng
    public function add_favorite(Request $request, $id)
    {
        if (!$this->isStudent($request)) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Bạn không có quyền để report tin tuyển dụng.");
        }

        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }

        $student = $request->user()->student;
        $checkFavorite = $student->favorite_news->where('recruitment_new_id', $id)->count();
        if ($checkFavorite > 0) {
            return AdapterHelper::sendResponse(false, 'reported', 400, "Bạn đã thêm tin tuyển dụng này vào danh sách yêu thích.");
        }

        FavoriteNews::create([
            'student_id' => $student->student_id,
            'recruitment_new_id' => $id,
        ]);

        return AdapterHelper::sendResponse(true, 'success', 200, "Thêm tin tuyển dụng vào danh sách yêu thích thành công.");
    }

    // id ở bảng favorite_news
    public function delete_favorite(Request $request, $id)
    {
        if (!$this->isStudent($request)) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Bạn không có quyền để report tin tuyển dụng.");
        }

        $favorite_news = FavoriteNews::where('student_id', $request->user()->student->student_id)
            ->where('recruitment_new_id', $id)
            ->first();

        if (!$favorite_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không có trong danh sách yêu thích.");
        }

        $favorite_news->delete();

        return AdapterHelper::sendResponse(true, 'success', 200, "Xoá tin tuyển dụng khỏi danh sách yêu thích thành công.");
    }

    public function isStudent(Request $request): bool
    {
        return $request->user()->role === 3;
    }

    public function isRecuiter(Request $request): bool
    {
        return $request->user()->role === 2;
    }

    // Lấy danh sách ứng viên ứng tuyển vào tin tuyển dụng
    public function get_students_apply_news(Request $request, $id)
    {
        if (!$this->isRecuiter($request)) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Bạn không có quyền xem danh sách ứng viên.");
        }

        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }

        $recruitment_news = Recruitment::query()
            ->join('recruitment_news', 'recruitment_news.id', 'recruitments.recruitment_new_id')
            ->join('students', 'students.student_id', 'recruitments.student_id')
            ->join('users', 'users.id', 'students.user_id')
            ->select('recruitments.*', 'students.student_id', 'users.name', 'users.avatar', 'users.email')
            ->where('recruitment_new_id', $id)
            ->paginate($request->per_page ?? config('app.per_page'));

        return AdapterHelper::sendResponsePaginating(true, $recruitment_news, 200, "success");
    }

    public function filter_news(Request $request)
    {
        $news = RecruitmentNews::query()->with('recruiter.user');

        if (isset($request->job_name)) {
            $news->where('job_name','like', '%'. $request->job_name. '%');
        }

        if (isset($request->company_name)) {
            $news->whereHas('recruiter', function($q) use ($request){
                $q->where('company_name','like', '%'. $request->company_name. '%');
            });
        }

        if (isset($request->province_id)) {
            $news->whereHas('recruiter', function($q) use ($request){
                $q->where('province_id', $request->province_id);
            });
        }

        if (isset($request->salary_type)) {
            if ($request->salary_type == 1) {
                $news->where('salary_type', 1);

                $news = isset($request->salary_from) && !isset($request->salary_to) ? $news->where('salary_from', '>=', $request->salary_from ?? 0) : $news;
                $news = isset($request->salary_to) && !isset($request->salary_from) && $request->salary_to > 0 ? $news->where('salary_to', '>=', $request->salary_to) : $news;
                $news = isset($request->salary_from) && isset($request->salary_to) && $request->salary_from <= $request->salary_to 
                    ? $news->where('salary_from', '>=', $request->salary_from ?? 0)
                        ->where('salary_to' , '<=', $request->salary_to)
                    : $news;
            } else if ($request->salary_type == 2) {
                $news->where('salary_type', 2);
            }
        }

        if (isset($request->min_age)) {
            $news->where('min_age', '>=', $request->min_age);
        }

        if (isset($request->sex)) {
            $news->where('sex', $request->sex);
        }

        $news = $news->paginate($request->per_page ?? config('app.per_page'));
        return AdapterHelper::sendResponsePaginating(true, $news, 200, "Lọc tin tuyển dụng thành công.");
    }
}
