<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\JobCategory;
use App\Traits\AdapterHelper;
use Illuminate\Http\Request;

class JobCategoryController extends Controller
{
    // lấy tất cả danh mục đã publish

    public function get_all()
    {
        $all = JobCategory::query()->where('is_publised', 1)->get();
        return AdapterHelper::sendResponse(true, $all , 200, "Lấy danh sách thành công.");
    }
}
