<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Models\Recruiter;
use App\Models\Student;
use App\Models\User;
use App\Traits\AdapterHelper;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    // apply for recruiter and studentd
    public function update_requirement_infos(Request $request)
    {
        $role = $request->user()->role; // string
        if (!isset($role) || !in_array($role, [2, 3])) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, 'type_role không hỗ trợ.');
        }

        $validator = Validator::make($request->all(), [
            'sex' => $role ==  3 ? 'required' : '',
            'age' => $role ==  3  ? 'required' : '',
            'phone' => 'required|min:10|max:11',
            'province' => 'required',
            'district' => 'required',
            'ward' => 'required',
            'company_name' => $role ==  2  ? 'required' : '',
            'company_image' =>  $role == 2 ? 'required' : '',// thiếu type file
            'provinceID' => 'required'
        ],
        [
            'sex.required' => 'Vui lòng chọn giới tính',
            'age.required' =>'Vui lòng chọn tuổi',
            'phone.required' => 'Vui lòng nhập số điện thoại hợp lệ',
            'phone.min' => 'Vui lòng nhập số điện thoại hợp lệ',
            'phone.max' => 'Vui lòng nhập số điện thoại hợp lệ',
            'company_name.required' => 'Vui lòng nhập tên cửa hàng/ công ty',
            'company_image.required' => 'Vui lòng chọn hình ảnh cửa hàng/ công ty',
        ]);

        if ($validator->fails()) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, $validator->errors()->first());
        }

        $check = Province::find($request->provinceID);
        if (!$check) {
            return AdapterHelper::sendResponse(false, 'province error', 400, "Không tìm thấy tỉnh thành này");
        }

        DB::beginTransaction();
        try {
            $user = $request->user();
            if ($role == 3) {
                if (isset($user->student)) {
                    return AdapterHelper::sendResponse(false, 'User have been update profile', 400, "User have been update profile");
                }

                $student = Student::create([
                    'sex' => $request->sex,
                    'age' => $request->age,
                    'phone' => $request->phone,
                    'address' => $request->ward . ', '. $request->district . ', '. $request->province,
                    'province_id' => $request->provinceID, //
                    'user_id' => $user->id
                ]);
            } else { // recruiter
                if (isset($user->recruiter)) {
                    return AdapterHelper::sendResponse(false, 'User have been update profile', 400, "User have been update profile");
                }
                $urlImage = AdapterHelper::upload_image($request->company_image, 'company_image', 'public');
                $recruiter = Recruiter::create([
                    'company_name' => $request->company_name,
                    'company_image' => $urlImage,
                    'phone' => $request->phone,
                    'company_address' => $request->ward . ', '. $request->district . ', '. $request->province,
                    'website' => $request->website ?? '',
                    'province_id' => $request->provinceID, //
                    'user_id' => $user->id
                ]);
            }

            if (isset($student) || isset($recruiter)) {
                DB::commit();
                return AdapterHelper::sendResponse(true, 'Cập nhật thông tin thành công', 200, "Cập nhật thông tin thành công");
            }

            return AdapterHelper::sendResponse(false, 'Cập nhật thất bại', 400, "Cập nhật thất bại");
        } catch (Exception $e) {
            DB::rollback();
            return AdapterHelper::sendResponse(false, 'Cập nhật thất bại', 400, "Cập nhật thất bại");
        }
    }


    // Cập nhật thông tin cá nhân
    public function update_info(Request $request)
    {
        $user = $request->user();
        $role = $user->role;
        if (!isset($role) || !in_array($role, [2, 3])) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, 'type_role không hỗ trợ.');
        }

        DB::beginTransaction();
        try {
            if (isset($request->name) && $request->name != '') {
                $user->name = $request->name;
            }
            
            if ($request->hasFile('avatar')) {
                if ($user->avatar) {
                    $arr = explode('/', $user->avatar);
                    $fileName = $arr[count($arr) - 1] ?? '';
                    if (Storage::disk('public')->exists('avatars/'. $fileName)) {
                        Storage::disk('public')->delete('avatars/'. $fileName);
                    }
                }

                $urlImage = AdapterHelper::upload_image($request->avatar, 'avatars', 'public');
                if ($urlImage == false) {
                    return AdapterHelper::sendResponse(false, 'image error', 400, 'Định dạng hình ảnh không hỗ trợ. Định dạng file hỗ trợ là .png, .jpeg, .jpg, .psd');
                }
                $user->avatar = $urlImage;
            }
            $user->save();
    
            if ($role == 2) {
                $recruiter = $user->recruiter;
                if ($request->hasFile('company_image')) {
                    if ($recruiter->company_image) {
                        $arr = explode('/', $recruiter->company_image);
                        $fileName = $arr[count($arr) - 1] ?? '';
                        if (Storage::disk('public')->exists('company_image/'. $fileName)) {
                            Storage::disk('public')->delete('company_image/'. $fileName);
                        }
                    }
                    $urlImage = AdapterHelper::upload_image($request->company_image, 'company_image', 'public');
                    $recruiter->company_image = $urlImage;
                    $recruiter->save();
                }
                $data_update = collect($request->only(['phone', 'company_name', 'website']));
    
                $data_update = $data_update->filter(function ($value) {
                    return $value != null;
                })->toArray();

                if (!empty($data_update)) {
                    $recruiter->update($data_update);
                }
            } else {
                $student = $user->student;
                $data_update = collect($request->only(['sex', 'age', 'phone']));
                
                $data_update = $data_update->filter(function ($value) {
                    return $value != null;
                })->toArray();

                if (!empty($data_update)) {
                    $student->update($data_update);
                }
            }

            DB::commit();
            return AdapterHelper::sendResponse(true, 'Success', 200, "Cập nhật thông tin thành công");
        } catch (\Throwable $th) {
            DB::rollback();
            return AdapterHelper::sendResponse(false, 'server error', 500, 'Đã xảy ra lỗi trong quá trình cập nhật.');
        }
        
    }


    public function update_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|min:8',
            'new_password' =>'required|min:8',
            'confirm_new_password' =>'required|min:8'
        ],
        [
            'old_password.required' => 'Vui lòng nhập mật khẩu cũ', 
            'new_password.required' => 'Vui lòng nhập mật khẩu mới', 
            'confirm_new_password.required' => 'Vui lòng nhập xác nhận mật khẩu', 
            'confirm_new_password.min' => 'Mật khẩu phải có ít nhất 8 kí tự', 
        ]);

        if ($validator->fails()) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, $validator->errors()->first());
        }

        if (!Hash::check($request->old_password, $request->user()->password)) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Mật khẩu cũ không chính xác.");
        }

        if ($request->new_password != $request->confirm_new_password) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Mật khẩu mới và nhập lại mật khẩu mới không trùng nhau.");
        }

        $request->user()->update([
            'password' => bcrypt($request->new_password)
        ]);

        return AdapterHelper::sendResponse(true, 'Success', 200, "Cập nhật mật khẩu thành công");
    }

    //https://api.ghn.vn/home/docs/detail?id=93
    //https://online-gateway.ghn.vn/shiip/public-api/master-data/province
    // token ghn: d0393187-b68e-11ec-935b-4e3cff801388
    // thêm province code
    function test() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://online-gateway.ghn.vn/shiip/public-api/master-data/province',
            CURLOPT_HTTPHEADER => ["token" => "d0393187-b68e-11ec-935b-4e3cff801388"]
        ));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('token:d0393187-b68e-11ec-935b-4e3cff801388'));

        $resp = curl_exec($curl);

        $resp = json_decode($resp, true);
        foreach ($resp['data'] as $value) {
            Province::create([
                'id' => $value['ProvinceID'],
                'name' => $value['ProvinceName']
            ]);
        }
        curl_close($curl);

        return AdapterHelper::sendResponse(true, 'Cập nhật thông tin thành công', 200, "Cập nhật thông tin thành công");
    }

    public function get_recruiter_info(Request $request, $id)
    {
        $recruiter = Recruiter::find($id);

        if (!$recruiter) {
            return AdapterHelper::sendResponse(false, 'not found', 400, 'Không tìm thấy nhà tuyển dụng.');
        }

        $recruiter = User::query()
            ->join('recruiters', 'users.id', '=', 'recruiters.user_id')
            ->where('recruiters.is_block', 0)
            ->find($recruiter->user_id);

        return AdapterHelper::sendResponse(true, $recruiter, 200, 'Success.');
    }

    public function get_student_info(Request $request, $id)
    {
        $student = Student::find($id);

        if (!$student) {
            return AdapterHelper::sendResponse(false, 'not found', 400, 'Không tìm thấy sinh viên.');
        }

        $student = User::query()
            ->join('students', 'users.id', '=', 'students.user_id')
            ->where('students.is_block', 0)
            ->find($student->user_id);

        return AdapterHelper::sendResponse(true, $student, 200, 'Success.');
    }

    public function get_user_info(Request $request)
    {
        $role = $request->user()->role;
        if (!isset($role) || !in_array($role, [1, 2, 3])) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, 'Không tìm thấy user.');
        }

        $user = null;

        if ($role == 1) {
            $user = User::query()
                ->with('admin')
                ->find($request->user()->id);
        } else if ($role == 2) {
            $user = User::query()
                ->join('recruiters', 'users.id', '=', 'recruiters.user_id')
                ->where('recruiters.is_block', 0)
                ->find($request->user()->id);
        } else if ($role == 3) {
            $user = User::query()
                ->join('students', 'users.id', '=', 'students.user_id')
                ->where('students.is_block', 0)
                ->find($request->user()->id);
        }

        return AdapterHelper::sendResponse(true, $user, 200, 'Success.');
    }

}
