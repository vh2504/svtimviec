<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FavoriteNews;
use App\Models\JobCategory;
use App\Models\Recruiter;
use App\Models\Recruitment;
use App\Models\RecruitmentNews;
use App\Models\ReportNews;
use App\Traits\AdapterHelper;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RecruitmentNewsController extends Controller
{

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'job_name' => 'required',
            'job_description' => 'required',
            'category_id' => 'required',
            'recruiter_id' => 'required',
            'salary_type' => 'required',
            'salary_from' => $request->salary_type == 1 ? 'required': '',
            'salary_to' => $request->salary_type == 1 ? 'required': '',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', "Vui lòng điền đầy đủ thông tin của tin tuyển dụng.");
        }

        DB::beginTransaction();
        try {
            $news = RecruitmentNews::create([
                'job_name' => $request->job_name,
                'job_description' => $request->job_description,
                'job_category_id' => $request->job_category_id,
                'salary_type' => $request->salary_type, // 1 hoặc 2 (cố định hoặc thoả thuận)
                'recruiter_id' => $request->recruiter_id,
                'status' => 0, // 0 chưa duyệt, 1 đã duyệt
                'min_age' => $request->min_age ?? null,
                'sex' => $request->sex ?? 2 // mặc định tạo ra không yêu cầu giới tính
            ]);

            if ($request->salary_type == 1) {
                $news->salary_from = $request->salary_from;
                $news->salary_to = $request->salary_to;
                $news->save();
            }
            DB::commit();
            return redirect()->route('admin.news.list')->with('success', "Thêm mới tin tuyển dụng thành công");
        } catch (Exception $e) {
            DB::rollback();
            AdapterHelper::write_log_error($e, 'api', 'create news');
            return redirect()->back()->with('error', "Có lỗi xảy ra");
        }
    }

    public function index(Request $request)
    {
        $this->checkLogin($request);

        $listRecruiter = Recruiter::query()
            ->with('user')
            ->where('is_block', 0)
            ->get();
        $listCategory = JobCategory::where('is_publised', 1)->get();

        $data = RecruitmentNews::query()
            ->with('recruiter.user')
            ->orderByDesc('created_at')
            ->paginate(config('app.per_page'));

        return view('admin.pages.recruitment-news.index', compact('data', 'listRecruiter', 'listCategory'));
    }

    public function approve(Request $request)
    {
        $user = $request->user();
        if ($user->role != 1) {
            return AdapterHelper::sendResponse(false, 'Validator error', 400, "Không có quyền xoá tin tuyển dụng.");
        }

        $id = $request->id;
        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return AdapterHelper::sendResponse(false, 'not found', 400, "Không tìm thấy tin tuyển dụng này.");
        }
        DB::beginTransaction();
        try {
            if ($recruitment_news->status == 1) {
                return AdapterHelper::sendResponse(false, 'error approve', 400, "Tin tuyển dụng này đã được duyệt.");
            }

            if (isset($recruitment_news->jobCategory->is_publised) && $recruitment_news->jobCategory->is_publised == 0) {
                $recruitment_news->jobCategory->is_publised = 1;
                $recruitment_news->jobCategory->save();
            }

            $recruitment_news->status = 1;
            $recruitment_news->admin_id = $user->admin->admin_id;
            $recruitment_news->save();

            DB::commit();
            return AdapterHelper::sendResponse(true, 'success', 200, "Duyệt tin tuyển dụng thành công.");

        } catch (\Exception $e) {
            DB::rollback();
            AdapterHelper::write_log_error($e, 'api', 'approve news');
            return AdapterHelper::sendResponse(false, 'error', 400, "Đã có lỗi xảy ra.");
        }
    }

    public function delete(Request $request, $id)
    {
        $this->checkLogin($request);
        $user = $request->user();

        $recruitment_news = RecruitmentNews::find($id);
        if (!$recruitment_news) {
            return redirect()->back()->with('error', "Không tìm thấy tin tuyển dụng này");
        }

        DB::beginTransaction();
        try {
            $favorite = FavoriteNews::where('recruitment_new_id', $id)->first();
            $favorite ? $favorite->delete() : '';

            $report = ReportNews::where('recruitment_new_id', $id)->first();
            $report ? $report->delete() : '';

            $recruitment_apply = Recruitment::where('recruitment_new_id', $id)->first();
            $recruitment_apply ? $recruitment_apply->delete() : '';

            $recruitment_news->delete();

            DB::commit();
            return redirect()->back()->with('success', "Xoá tin tuyển dụng thành công");
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', "Có lỗi xảy ra");
        }
    }


    private function checkLogin($request)
    {
        $user = $request->user();
        if ($user->role != 1) {
            Auth::logout();
            return redirect()->route('login');
        }
    }
}
