<?php

namespace App\Http\Middleware;

use App\Traits\AdapterHelper;
use Closure;
use Illuminate\Http\Request;

class CheckUserLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $check = true;
        $user = $request->user();
        if ($request->is('*/update-requirement-infos')) {
            return $next($request);
        }

        switch ($user->role) {
            case 2:
                if (!$user->recruiter) {
                    return AdapterHelper::sendResponse(false, 'miss info', 400, "Bạn cần cập nhật đầy đủ thông tin để sử dụng ứng dụng.");
                }

                if ($user->recruiter->is_block == 1) {
                    $check = false;
                }

                break;
            case 3:
                if (!$user->student) {
                    return AdapterHelper::sendResponse(false, 'miss info', 400, "Bạn cần cập nhật đầy đủ thông tin để sử dụng ứng dụng.");
                }

                if ($user->student->is_block == 1) {
                    $check = false;
                }
                break;
        }
        if (!$check) {
            return AdapterHelper::sendResponse(false, 'blocked account', 400, "Tài khoản của bạn đã bị khoá.");
        }

        return $next($request);
    }
}
