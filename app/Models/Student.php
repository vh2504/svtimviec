<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $table = 'students';

    protected $primaryKey = 'student_id';

    protected $fillable = [
        'sex',
        'age',
        'phone',
        'address',
        'is_block',
        'province_id',
        'user_id'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function apply_recruitment_news()
    {
        return $this->belongsToMany(RecruitmentNews::class
            , 'recruitments', 'student_id'
            , 'recruitment_new_id')
            ->withTimestamps();
    }

    public function report_news()
    {
        return $this->hasMany(ReportNews::class, 'student_id', 'student_id');
    }

    public function favorite_news()
    {
        return $this->hasMany(FavoriteNews::class, 'student_id', 'student_id');
    }
}
