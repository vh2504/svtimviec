<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavoriteNews extends Model
{
    use HasFactory;

    protected $table = 'favorite_news';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public $timestamps = true;

    protected $casts = [
        'verified_at' => 'datetime',
        'verify_expired' => 'datetime'
    ];

}
