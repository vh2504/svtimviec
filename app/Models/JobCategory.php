<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobCategory extends Model
{
    use HasFactory;

    protected $table = 'job_categories';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public $timestamps = false;

    public function recruitmentNew()
    {
        return $this->hasMany(RecruitmentNews::class, 'job_category_id', 'id');
    }

}
