<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecruitmentNews extends Model
{
    use HasFactory;

    protected $table = 'recruitment_news';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public $timestamps = true;

    protected $casts = [
        'created_at' => 'datetime',
        'update_at' => 'datetime'
    ];

    /**
     * salary_type
     * 1 - cố định
     * 2 - thoả thuận
     */

    public function jobCategory()
    {
        return $this->belongsTo(JobCategory::class, 'job_category_id', 'id');
    }

    public function recruiter()
    {
        return $this->belongsTo(Recruiter::class, 'recruiter_id', 'recruiter_id');
    }

    public function report_news()
    {
        return $this->hasMany(ReportNews::class, 'recruitment_new_id', 'id');
    }

    public function favorite_news()
    {
        return $this->hasMany(FavoriteNews::class, 'recruitment_new_id', 'id');
    }

    public function apply_news()
    {
        return $this->hasMany(Recruitment::class, 'recruitment_new_id', 'id');
    }

    public function students_apply()
    {
        return $this->belongsToMany(Student::class
            , 'recruitments', 'recruitment_new_id'
            , 'student_id')
            ->with('user')
            ->withTimestamps();
    }
}
