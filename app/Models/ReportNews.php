<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ReportNews extends Model
{
    use HasFactory;

    protected $table = 'report_news';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public $timestamps = true;

    protected $casts = [
        'verified_at' => 'datetime',
        'verify_expired' => 'datetime'
    ];
}
