<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use App\Traits\AdapterHelper;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        // return parent::render($request, $exception);
        if ($request->is('api/*') && $exception->getMessage() !== 'Unauthenticated.') {
            AdapterHelper::write_log_error($exception, "Mobile", $request->getRequestUri());
            try {
                //code...
                if ($exception->getStatusCode() == 403) {
                    return AdapterHelper::sendResponse(false, 'Permission denied', 403, $exception->getMessage());
                }
            } catch (\Throwable $th) {
                //throw $th;
                return AdapterHelper::sendResponse(false, 'Server error:', 500, $exception->getMessage());
                // return AdapterHelper::sendResponse(false,'Undefined',400,$th->getMessage());
            }

            return AdapterHelper::sendResponse(false, 'Server error', 500, $exception->getMessage());
        } elseif ($request->is('cms/*')) {
            AdapterHelper::write_log_error($exception, "Web", $request->getRequestUri());
        }
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->is('api/*'))
            return AdapterHelper::sendResponse(false, "Unauthenticated", 401, "Access token expired, revoked or wrong");
        else return redirect()->guest(route('login'));
    }
}
