<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSalaryFieldIntoRecruitmentNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recruitment_news', function($table) {
            $table->unsignedDecimal('salary_from', 10, 2)->nullable()->change();
            $table->unsignedDecimal('salary_to', 10, 2)->nullable()->change();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recruitment_news', function($table) {
            $table->float('salary_from')->nullable()->change();
            $table->float('salary_to')->nullable()->change();
        });
    }
}
