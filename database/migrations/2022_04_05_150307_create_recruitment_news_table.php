<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecruitmentNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recruitment_news', function (Blueprint $table) {
            $table->id();
            $table->string('job_name')->nullable();
            $table->text('job_description')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->tinyInteger('salary_type')->nullable();
            $table->float('salary_from')->nullable();
            $table->float('salary_to')->nullable();
            $table->tinyInteger('min_age')->nullable();
            $table->tinyInteger('sex')->nullable();
            $table->unsignedBigInteger('recruiter_id')->nullable();
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->unsignedBigInteger('job_category_id')->nullable();
            $table->timestamps();

            $table->foreign('recruiter_id')->references('recruiter_id')->on('recruiters');
            $table->foreign('admin_id')->references('admin_id')->on('admins');
            $table->foreign('job_category_id')->references('id')->on('job_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recruitment_news');
    }
}
