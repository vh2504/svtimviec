@extends('admin.layouts.master')

@section('content')
    <div class="row px-2 py-1" style="font-size: 18px">
        <div class="col-sm-12">
            @if (session()->has('error'))
                <script>
                    alert("{{session()->get('error')}}");
                </script>
            @endif

            @if (session()->has('success'))
                <script>
                    alert("{{session()->get('success')}}");
                </script>
            @endif
        </div>
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Quản lý tin tuyển dụng</h3>
                <div class="btn btn-primary" data-toggle="modal" data-target="#modal-add">Thêm mới</div>
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                            <tr>
                                <th class="border-top-0">#</th>
                                <th class="border-top-0">Tên công việc</th>
                                <th class="border-top-0">Trạng thái</th>
                                <th class="border-top-0">Ngày đăng</th>
                                <th class="border-top-0">Tên công ty</th>
                                <th class="border-top-0"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($data as $news)
                                <tr>
                                    <td>{{$news->id}}</td>
                                    <td>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal{{$news->id}}">{{$news->job_name}}</a>
                                    </td>
                                    <td>
                                        <span class="badge {{$news->status == 1  ? 'badge-success' : 'badge-secondary'}}">
                                            {{$news->status == 1 ? 'Đã duyệt' : 'Chờ duyệt'}}
                                        </span>
                                    </td>
                                    <td>{{ isset($news->created_at) ? date('d/m/Y', strtotime($news->created_at)) : ""}}</td>
                                    <td>{{$news->recruiter->company_name ?? ''}}</td>
                                    <td>
                                        @if ($news->status == 0 )
                                            <a href="javascript:void(0)" class="approve-news badge badge-info">Duyệt tin</a>  
                                            <input type="hidden" class="id-approve" value="{{$news->id}}">
                                        @endif
                                        <a href="{{route('admin.news.delete', ['id' => $news->id])}}" class="badge badge-warning">Xoá</a>  
                                    </td>
                                </tr>

                                <!-- The Modal -->
                                <div class="modal" id="myModal{{$news->id}}">
                                    <div class="modal-dialog modal-dialog-centered " style="max-width: 800px">
                                        <div class="modal-content">
                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h4 class="modal-title">Chi tiết tin tuyển dụng</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            
                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <div class="row px-2 py-1" style="font-size: 18px">
                                                    <div class="col-3">Tên công việc</div>
                                                    <div class="col-9">{{$news->job_name}}</div>
                                                </div>
                                                <div class="row px-2 py-1" style="font-size: 18px">
                                                    <div class="col-3">Mô tả công việc</div>
                                                    <div class="col-9">{{$news->job_description}}</div>
                                                </div>
                                                <div class="row px-2 py-1" style="font-size: 18px">
                                                    <div class="col-3">Trạng thái</div>
                                                    <div class="col-9" id="status">
                                                        <span class="badge {{$news->status == 1  ? 'badge-success' : 'badge-secondary'}}">
                                                            {{$news->status == 1 ? 'Đã duyệt' : 'Chờ duyệt'}}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row px-2 py-1" style="font-size: 18px">
                                                    <div class="col-3">Mức lương</div>
                                                    <div class="col-9">
                                                        @if ($news->salary_type == 1)
                                                            Từ: {{number_format($news->salary_from, 0)}} đ<br>
                                                            Đến: {{number_format($news->salary_to, 0)}} đ
                                                        @else
                                                            Thoả thuận
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="row px-2 py-1" style="font-size: 18px">
                                                    <div class="col-3">Độ tuổi</div>
                                                    <div class="col-9">
                                                        {{ isset($news->min_age) ? "Từ ". $news->min_age : "Không yêu cầu"}}
                                                    </div>
                                                </div>
                                                <div class="row px-2 py-1" style="font-size: 18px">
                                                    <div class="col-3">Giới tính</div>
                                                    <div class="col-9">
                                                        @isset($news->sex)
                                                            {{ $news->sex == 1 ? "Nam" : ($news->sex == 0) ? "Nữ" : "Không yêu cầu"}}
                                                        @endisset
                                                        {{ !isset($news->sex) ? "Không yêu cầu" : ""}}
                                                    </div>
                                                </div>
                                                <div class="row px-2 py-1" style="font-size: 18px">
                                                    <div class="col-3">Ngày đăng</div>
                                                    <div class="col-9">{{ isset($news->created_at) ? date('d/m/Y', strtotime($news->created_at)) : ""}}</div>
                                                </div>
                                                <div class="row px-2 py-1" style="font-size: 18px">
                                                    <div class="col-3">Tên nhà tuyển dụng</div>
                                                    <div class="col-9">
                                                        {{$news->recruiter->user->name ?? ""}}
                                                    </div>
                                                </div>
                                                <div class="row px-2 py-1" style="font-size: 18px">
                                                    <div class="col-3">Tên công ty</div>
                                                    <div class="col-9">
                                                        {{$news->recruiter->company_name ?? ""}}
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <!-- Modal footer -->
                                            <div class="modal-footer">
                                                @if (!isset($news->status) || $news->status == 0)
                                                    <button type="button" class="btn btn-warning text-white approve-news">Duyệt tin</button>
                                                    <input type="hidden" class="id-approve" value="{{$news->id}}">
                                                @endif
                                                <button type="button" class="btn btn-danger text-white" data-dismiss="modal">Đóng</button>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>

                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="text-center">
                {{$data->links()}}
            </div>
        </div>
        
    </div>

    <div class="modal" id="modal-add">
        <form action="{{route('admin.news.create')}}" method="post">
            @csrf
            <div class="modal-dialog " style="max-width: 1000px">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Thêm mới tin tuyển dụng</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row px-2 py-1" style="font-size: 18px">
                            <div class="col-3">Nhà tuyển dụng</div>
                            <div class="col-9">
                                <select class="form-control" name="recruiter_id">
                                    <option disabled selected>Chọn nhà tuyển dụng - tên cửa hàng</option>
                                    @foreach ($listRecruiter as $recruiter)
                                        <option value="{{$recruiter->recruiter_id}}">{{$recruiter->user->name . " - " . $recruiter->company_name}}</option>
                                    @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="row px-2 py-1" style="font-size: 18px">
                            <div class="col-3">Danh mục công việc</div>
                            <div class="col-9">
                                <select class="form-control" name="category_id">
                                    <option disabled selected>Chọn danh mục công việc</option>
                                    @foreach ($listCategory as $cate)
                                        <option value="{{$cate->id}}">{{$cate->name}}</option>
                                    @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="row px-2 py-1" style="font-size: 18px">
                            <div class="col-3">Tên công việc</div>
                            <div class="col-9">
                                <input type="text" class="form-control" name="job_name" required>
                            </div>
                        </div>
    
                        <div class="row px-2 py-1" style="font-size: 18px">
                            <div class="col-3">Mô tả công việc</div>
                            <div class="col-9">
                                <textarea class="form-control" name="job_description" id="" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        
                        <div class="row px-2 py-2" style="font-size: 18px">
                            <div class="col-3">Yêu cầu giới tính</div>
                            <div class="col-9">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="sex" value="2">Không yêu cầu
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="sex" value="1">Nam
                                    </label>
                                </div>
                                <div class="form-check-inline disabled">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="sex" value="0">Nữ
                                    </label>
                                </div>
                            </div>
                        </div>
    
                        <div class="row px-2 py-1" style="font-size: 18px">
                            <div class="col-3">Yêu cầu độ tuổi</div>
                            <div class="col-9">
                                <input min="0" class="form-control" type="number" name="min_age">
                            </div>
                        </div>
    
                        <div class="row px-2 py-2" style="font-size: 18px">
                            <div class="col-3">Mức lương chi trả </div>
                            <div class="col-9">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" id="checkSalary1" class="form-check-input" name="salary_type" value="1">Cố định
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" id="checkSalary2" class="form-check-input" name="salary_type" value="2">Thoả thuận
                                    </label>
                                </div>
                                <div class="row d-none" id="showSalary1">
                                    <div class="form-group col-md-6">
                                        <input type="number" class="form-control" id="ak" placeholder="Từ" name="salary_from">
                                    </div>
                                      <div class="form-group col-md-6">
                                        <input type="number" class="form-control" id="dd" placeholder="Đến" name="salary_to">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info text-white">Thêm mới</button>
                        <button type="button" class="btn btn-danger text-white" data-dismiss="modal">Huỷ bỏ</button>
                    </div>
                </div>
            </div>
        </form>
       
    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.approve-news').on('click', function() {
                let id = $(this).siblings('.id-approve').val();
                let _token = $('meta[name="csrf-token"]').attr('content');
                
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ route('admin.news.approve') }}",
                    data: { _token:_token, id: id},

                    success: function(data){
                        console.log(data);
                        if (data.status == true) {
                            alert(data.message);
                            window.location.reload();
                        }
                    }, 
                    error: function(data) {
                        alert(data.message);
                    }
                    
                });
               
            });
            

            $('#checkSalary1').on('click', function() {
                $('#showSalary1').removeClass('d-none');
            });
            $('#checkSalary2').on('click', function() {
                $('#showSalary1').addClass('d-none');
            });
        });
    </script>
@endsection