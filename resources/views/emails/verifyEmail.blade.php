@component('mail::message')
# Kính gửi {{$user->name}}

Cám ơn {{$user->name}} đã đăng ký tài khoản tại ứng dụng {{ config('app.name') }} của chúng tôi.
<br>
Đây là mã xác thực tài khoản của bạn:
<br>
## {{ $user->verify_code }}
Lưu ý: Mã xác thực sẽ hết hạn vào lúc: {{date('H:i:s d/m/Y', strtotime($user->verify_expired))}}

Thân mến,<br>
{{ config('app.name') }}
@endcomponent
