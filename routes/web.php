<?php

use App\Http\Controllers\Admin\RecruitmentNewsController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

// Will fix after creating admin site
Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login'])->name('login');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('/admin')->name('admin.')->group(function () {

        Route::prefix('/recruitment-news')->name('news.')->group(function () {
            Route::get('/', [RecruitmentNewsController::class, 'index'])->name('list');
            Route::get('/delete/{id}', [RecruitmentNewsController::class, 'delete'])->name('delete');
            Route::post('/approve', [RecruitmentNewsController::class, 'approve'])->name('approve');
            Route::post('/create', [RecruitmentNewsController::class, 'create'])->name('create');
        });

    });
});

Route::get('/mailable', function () {
    $user = App\Models\User::find(1);

    Mail::to('dangvanhoa077@gmail.com')->send(new App\Mail\VerifyEmail($user));
    return new App\Mail\VerifyEmail($user);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');