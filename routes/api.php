<?php

use App\Http\Controllers\API\Admin\AdminController;
use App\Http\Controllers\API\Admin\CategoryController;
use App\Http\Controllers\API\Admin\NewsController;
use App\Http\Controllers\API\Admin\RecruiterController;
use App\Http\Controllers\API\Admin\ReportNewsController;
use App\Http\Controllers\API\Admin\StudentController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\JobCategoryController;
use App\Http\Controllers\API\RecruitmentNewsController;
use App\Http\Controllers\API\UserController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login'])->name('Auth.login');
    Route::post('register', [AuthController::class, 'register'])->name('Auth.register');
    Route::post('confirm-email', [AuthController::class, 'confirm_email'])->name('Auth.confirmEmail');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('logout', [AuthController::class, 'logout'])->name('Auth.logout');
    });
});


Route::group(['middleware' => ['auth:api', 'check_user']], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::post('update-requirement-infos', [UserController::class, 'update_requirement_infos'])->name('User.updateInfo');
        Route::get('recruiter/{id}', [UserController::class, 'get_recruiter_info'])->name('User.recruiterInfo');
        Route::get('student/{id}', [UserController::class, 'get_student_info'])->name('User.studentInfo');
        Route::get('/info', [UserController::class, 'get_user_info'])->name('User.userInfo');
        Route::post('update-info', [UserController::class, 'update_info']);
        Route::post('update-password', [UserController::class, 'update_password']);
    });

    Route::group(['prefix' => 'recruitment-news'], function () {
        Route::get('/', [RecruitmentNewsController::class, 'getAll'])->name('News.getAll');
        Route::get('/detail/{id}', [RecruitmentNewsController::class, 'detail'])->name('News.detail'); // detail
        Route::post('create', [RecruitmentNewsController::class, 'create'])->name('News.create');
        Route::get('history-news', [RecruitmentNewsController::class, 'get_history_news']);
        Route::get('detail-news/{id}', [RecruitmentNewsController::class, 'detail_news']);
        Route::delete('delete-news/{id}', [RecruitmentNewsController::class, 'delete_news']);
        Route::put('edit/{id}', [RecruitmentNewsController::class, 'edit']);

        Route::post('apply', [RecruitmentNewsController::class, 'apply'])->name('News.apply');
        Route::get('list-apply/{id}', [RecruitmentNewsController::class, 'get_students_apply_news'])->name('News.list-apply');
        Route::post('report/{id}', [RecruitmentNewsController::class, 'report'])->name('News.report');

        Route::post('add-favorite/{id}', [RecruitmentNewsController::class, 'add_favorite'])->name('News.favorite');
        Route::get('favorite-news', [RecruitmentNewsController::class, 'get_all_favorite_news'])->name('News.getAllFavorite');
        Route::delete('delete-favorite/{id}', [RecruitmentNewsController::class, 'delete_favorite'])->name('News.delete-favorite');

        Route::get('filter-news', [RecruitmentNewsController::class, 'filter_news']);
    });

    Route::get('get-all-category', [JobCategoryController::class, 'get_all']);

    Route::group(['prefix' => 'admin'], function () {
        Route::get('get-info', [AdminController::class, 'get_admin_info'])->name('admin.get-info');
        Route::post('add-admin', [AdminController::class, 'add_admin']);

        //nhà tuyển dụng
        Route::post('add-recruiter', [RecruiterController::class, 'create']);
        Route::get('get-info-recruiter/{id}', [RecruiterController::class, 'get_info_recruiter']);
        Route::get('delete-recruiter/{id}', [RecruiterController::class, 'delete']);
        Route::get('block-recruiter/{id}', [RecruiterController::class, 'block']);
        Route::get('get-all-recruiters', [RecruiterController::class, 'get_all']);

        //Sinh viên
        Route::post('add-student', [StudentController::class, 'create']);
        Route::get('get-info-student/{id}', [StudentController::class, 'get_info_student']);
        Route::get('delete-student/{id}', [StudentController::class, 'delete']);
        Route::get('block-student/{id}', [StudentController::class, 'block']);
        Route::get('get-all-students', [StudentController::class, 'get_all']);

        //tin tuyển dụng
        Route::get('recruitment-news', [NewsController::class, 'getAll'])->name('admin.News.getAll');
        Route::get('recruitment-news/detail/{id}', [NewsController::class, 'detail'])->name('admin.News.detail');
        Route::delete('recruitment-news/delete/{id}', [NewsController::class, 'delete'])->name('admin.News.delete');
        Route::get('recruitment-news/approve/{id}', [NewsController::class, 'approve'])->name('admin.News.approve');

        // danh mục công việc
        Route::get('categories', [CategoryController::class, 'getAll'])->name('admin.category.getAll');
        Route::post('categories/create', [CategoryController::class, 'create'])->name('admin.category.create');
        Route::get('categories/detail/{id}', [CategoryController::class, 'detail'])->name('admin.category.detail');
        Route::delete('categories/delete/{id}', [CategoryController::class, 'delete'])->name('admin.category.delete');

        // tin bị report
        Route::get('report-news', [ReportNewsController::class, 'getAll']);
        Route::get('report-news/detail/{id}', [ReportNewsController::class, 'detail']);
        Route::delete('report-news/delete/{id}', [ReportNewsController::class, 'delete_report'])->name('admin.News.delete');
    });


});

// Route::get('/update-info-province', [UserController::class, 'test']);